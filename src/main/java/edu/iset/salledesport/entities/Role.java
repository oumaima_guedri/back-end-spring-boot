package edu.iset.salledesport.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Role implements Serializable {
	@Id
	private int id; 
	private String titre;
	
	
	@OneToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE},mappedBy="role")
	private List<Utilisateur> utilisateurs;
	
	
	
	public List<Utilisateur> getUtilisateur() {
		return utilisateurs;
	}
	public void setUtilisateur(List<Utilisateur> utilisateur) {
		this.utilisateurs = utilisateur;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	

}
